import java.util.ArrayList;

public class Parking {

    public static int createParking(int number) {
        return (int) (Math.random() * (((number / 3) - 0) + 1)) + 0;
    }

    public static int checkMoves(ArrayList<Car> cars, int number) {
        int min = 11;
        for (int i = 0; i < number; i++) {
            if (cars.get(i).getMoves() < min) {
                min = cars.get(i).getMoves();
            }
            if (cars.get(i) == null) {
                min = 0;
            }
        }
        return min;
    }

    public static void systemOut(ArrayList<Car> normalCars, ArrayList<Car> bigCars, int numberNormal, int numberBig) {
        int count = 0;
        System.out.println("Parking for normal cars:");
        for (int i = 0; i < numberNormal - 3; i++) {
            if ((normalCars.get(i) != null)&&(normalCars.get(i+1) != null)){
                if (normalCars.get(i).getId() != normalCars.get(i + 1).getId()) {
                    System.out.println("Normal car: id" + normalCars.get(i).getId());
                    System.out.println("Will be free in " + normalCars.get(i).getMoves());
                } else {
                    System.out.println("Big car: id" + normalCars.get(i).getId());
                    System.out.println("Will be free in " + normalCars.get(i).getMoves());
                    i++;
                }
                count++;
            }
        }
       if ((normalCars.get(numberNormal-1) != null)&&(normalCars.get(numberNormal).getId() != normalCars.get(numberNormal - 1).getId()) && (normalCars.get(numberNormal) != null)) {
               System.out.println("Normal car: id" + normalCars.get(numberNormal).getId());
               System.out.println("Will be free in " + normalCars.get(numberNormal).getMoves());
        }
       System.out.println("Parking for big cars:");
        for (int i = 0; i < numberBig; i++) {
            if (bigCars.get(i) != null) {
                System.out.println("Big car: id" + bigCars.get(i).getId());
                System.out.println("Will be free in " + bigCars.get(i).getMoves());
                count++;
            }
        }
        if (count == 0) {
            System.out.println("As you can see - empty(");
        }
    }

    public static int checkOccupied(ArrayList<Car> cars, int number) {
        int count = 5;
        for (int i = 0; i < number; i++) {
            if (cars.get(i) == null) {
                count = 0;
            }
        }
        return count;
    }
}
