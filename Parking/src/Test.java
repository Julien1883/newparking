import java.util.ArrayList;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        boolean isForever = true;
        int id = 1000;
        int temp;
        System.out.println("Enter number of parking spaces for normal car");
        Scanner scanner = new Scanner(System.in);
        int numberNormalCar = scanner.nextInt();
        System.out.println("Enter number of parking spaces for big car");
        int numberBigCar = scanner.nextInt();
        int newNumberNormal = Parking.createParking(numberNormalCar + numberBigCar);
        int newNumberBig = Parking.createParking(numberNormalCar + numberBigCar);
        ArrayList<Car> normalCars = new ArrayList<>();
        ArrayList<Car> bigCars = new ArrayList<>();
        bigCars.ensureCapacity(numberBigCar+5);
        normalCars.ensureCapacity(numberNormalCar+5);
        for (int i = 0; i < numberNormalCar; i++) {
            if (i < newNumberNormal) {
                normalCars.add(i, new Car(id, (int) ((Math.random() * ((10 - 0) + 1)) + 1)));
                id++;
            } else {
                normalCars.add(i, null);
            }
        }
        for (int i = 0; i < numberBigCar; i++) {
            if (i < newNumberBig) {
                bigCars.add(i, new Car(id, (int) ((Math.random() * ((10 - 0) + 1)) + 1)));
                id++;
            } else {
                bigCars.add(i, null);
            }
        }
        while (isForever) {
            int count = 0;
            for (int i = 0; i < numberBigCar; i++) {
                if (bigCars.get(i) != null) {
                    temp = bigCars.get(i).getMoves();
                    bigCars.get(i).setMoves(temp - 1);
                    if (bigCars.get(i).getMoves() < 1) {
                        bigCars.set(i, null);
                    }
                }
            }
            for (int i = 0; i < numberNormalCar-1; i++) {
                if (normalCars.get(i) != null) {
                    if (normalCars.get(i) != normalCars.get(i + 1)) {
                        temp = normalCars.get(i).getMoves();
                        normalCars.get(i).setMoves(temp - 1);
                        if (normalCars.get(i).getMoves() < 1) {
                            normalCars.set(i, null);
                        }
                    } else {
                        temp = normalCars.get(i).getMoves();
                        normalCars.get(i).setMoves(temp - 1);
                        if (normalCars.get(i).getMoves() < 1) {
                            normalCars.set(i, null);
                        }
                        i++;
                    }
                }
            }
            newNumberNormal = Parking.createParking(numberNormalCar + numberBigCar);
            newNumberBig = Parking.createParking(numberNormalCar + numberBigCar);
            for (int i = 0; i < numberBigCar; i++) {
                if (bigCars.get(i) == null) {
                    count++;
                    if (count > newNumberBig) {
                        break;
                    }
                    bigCars.set(i, new Car(id, (int) ((Math.random() * ((10 - 0) + 1)) + 1)));
                    id++;
                }
            }
            if (count <= newNumberBig) {
                for (int i = 0; i < numberNormalCar - 2; i++) {
                    count++;
                    if (count > newNumberBig) {
                        break;
                    }
                    if ((normalCars.get(i) == null) && (normalCars.get(i + 1) == null)) {
                        int t = (int) ((Math.random() * ((10 - 0) + 1)) + 1);
                        normalCars.set(i, new Car(id, t));
                        normalCars.set(i, new Car(id, t));
                        id++;
                    }
                }
            }
            count = 0;
            for (int i = 0; i < numberNormalCar; i++) {
                if (normalCars.get(i) == null) {
                    count++;
                    if (count > newNumberNormal) {
                        break;
                    }
                    normalCars.set(i, new Car(id, (int) ((Math.random() * ((10 - 0) + 1)) + 1)));
                    id++;
                }
            }

            if (Parking.checkOccupied(normalCars, numberNormalCar) != 0) {
                System.out.println("We haven't got parking spaces for normal cars. The place will be free in " + Parking.checkMoves(normalCars, numberNormalCar));
            }
            if (Parking.checkOccupied(bigCars, numberBigCar) != 0) {
                System.out.println("We haven't got parking spaces for big cars on big cars parking. The place will be free in " + Parking.checkMoves(bigCars, numberBigCar));
            }
            System.out.println("You can: complete the turn(tub something), clear the parking(clear) and");
            System.out.println("find out how many places occupied, free, when the nearest will be free(find)");
            String ourMove = scanner.next();
            while ((ourMove.equalsIgnoreCase("clear")) || (ourMove.equalsIgnoreCase("find"))) {
                if (ourMove.equalsIgnoreCase("clear")) {
                    for (int i = 0; i < numberNormalCar; i++) {
                        normalCars.set(i, null);
                    }
                    for (int i = 0; i < numberBigCar; i++) {
                        bigCars.set(i, null);
                    }
                } else {
                    Parking.systemOut(normalCars, bigCars, numberNormalCar, numberBigCar);
                }
                ourMove = scanner.next();
            }
        }
    }
}

