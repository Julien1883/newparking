public class Car {
    public int id;
    public int moves;

    public Car(int id, int moves) {
        this.id = id;
        this.moves = moves;
    }


    public void setMoves(int moves) {
        this.moves = moves;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMoves() {
        return moves;
    }

    public int getMoves(Object car) {
        return moves;
    }

    public int getId(Object car) {
        return id;
    }

}
